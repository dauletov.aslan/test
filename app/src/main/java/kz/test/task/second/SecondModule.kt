package kz.test.task.second

import kz.test.task.di.InjectionModule
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.module.Module
import org.koin.dsl.module

object SecondModule : InjectionModule {

    override fun create(): Module = module {
        viewModel { SecondViewModel(get()) }
    }
}