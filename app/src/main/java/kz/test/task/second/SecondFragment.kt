package kz.test.task.second

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import kz.test.task.R
import kz.test.task.databinding.FragmentSecondBinding
import kz.test.task.second.adapter.ListAdapter
import kz.test.task.viewbinding.viewBinding
import org.koin.androidx.viewmodel.ext.android.viewModel

class SecondFragment : Fragment(R.layout.fragment_second) {

    private val binding: FragmentSecondBinding by viewBinding()
    private val viewModel: SecondViewModel by viewModel()

    private val adapter: ListAdapter by lazy { ListAdapter() }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        with(binding) {
            recyclerView.layoutManager = LinearLayoutManager(requireContext())
            recyclerView.adapter = adapter

            viewModel.loadingLiveData.observe(viewLifecycleOwner, Observer { isLoading ->
                progressBar.isVisible = isLoading
            })

            viewModel.itemsLiveData.observe(viewLifecycleOwner, Observer { items ->
                adapter.setItems(items)
            })
        }
    }
}