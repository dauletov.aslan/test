package kz.test.task.second

import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.launch
import kz.test.task.base.BaseViewModel
import kz.test.task.first.domain.FirstInteractor
import kz.test.task.second.adapter.Item

class SecondViewModel(
    private val firstInteractor: FirstInteractor
) : BaseViewModel() {

    val itemsLiveData = MutableLiveData<List<Item>>()
    val loadingLiveData = MutableLiveData<Boolean>()

    init {
        loadItems()
    }

    private fun loadItems() {
        launch {
            loadingLiveData.value = true
            val posts = firstInteractor.getPosts()

            val items = posts.map { post ->
                val photo = firstInteractor.getPhoto(post.id)
                Item(post.id, post.title, post.body, photo.url)
            }

            itemsLiveData.value = items
            loadingLiveData.value = false
        }
    }
}