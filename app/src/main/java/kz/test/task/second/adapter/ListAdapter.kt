package kz.test.task.second.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import com.bumptech.glide.Glide
import kz.test.task.R
import kz.test.task.base.adapter.BaseAdapter
import kz.test.task.base.adapter.BaseViewHolder
import kz.test.task.databinding.ItemListBinding

class ListAdapter : BaseAdapter<Item>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<Item> {
        return ListItemViewHolder(
            ItemListBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
        )
    }

    class ListItemViewHolder(
        private val binding: ItemListBinding
    ) : BaseViewHolder<Item>(binding.root) {

        override fun onBind(item: Item) {
            super.onBind(item)
            with(binding) {
                Glide.with(itemView)
                    .load(item.photoUrl)
                    .placeholder(R.drawable.ic_picture)
                    .into(imageView)
                titleTextView.text = item.postTitle
                descriptionTextView.text = item.postBody
            }
        }
    }
}