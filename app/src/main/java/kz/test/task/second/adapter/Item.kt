package kz.test.task.second.adapter

data class Item(
    val postId: String,
    val postTitle: String,
    val postBody: String,
    val photoUrl: String
)