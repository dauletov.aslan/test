package kz.test.task.third.data

import retrofit2.http.GET
import retrofit2.http.Path

interface ThirdApi {
    @GET("users/{id}")
    suspend fun getUser(@Path("id") id: String): UserInfo
}