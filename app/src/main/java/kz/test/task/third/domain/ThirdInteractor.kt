package kz.test.task.third.domain

import kz.test.task.third.data.ThirdRemoteGateway

class ThirdInteractor(
    private val thirdRemoteGateway: ThirdRemoteGateway
) {

    suspend fun getUser(id: String): User = thirdRemoteGateway.getUser(id)
}