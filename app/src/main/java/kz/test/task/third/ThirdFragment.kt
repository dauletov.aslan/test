package kz.test.task.third

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import kz.test.task.R
import kz.test.task.databinding.FragmentThirdBinding
import kz.test.task.viewbinding.viewBinding
import org.koin.androidx.viewmodel.ext.android.viewModel

class ThirdFragment : Fragment(R.layout.fragment_third) {

    private val binding: FragmentThirdBinding by viewBinding()
    private val viewModel: ThirdViewModel by viewModel()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        with(binding) {
            viewModel.userInfoLiveData.observe(viewLifecycleOwner, Observer { user ->
                imageView.setImageResource(R.drawable.ic_user)
                titleTextView.text = user.name
                descriptionTextView.text = user.fullInfo
                phoneTextView.text = user.phone
            })
            viewModel.loadingLiveData.observe(viewLifecycleOwner, Observer { isLoading ->
                progressBar.isVisible = isLoading
            })
        }
    }
}