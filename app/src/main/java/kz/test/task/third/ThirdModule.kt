package kz.test.task.third

import kz.test.task.di.InjectionModule
import kz.test.task.third.data.ThirdApi
import kz.test.task.third.data.ThirdRemoteGateway
import kz.test.task.third.domain.ThirdInteractor
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.module.Module
import org.koin.dsl.bind
import org.koin.dsl.module
import retrofit2.Retrofit

object ThirdModule : InjectionModule {

    override fun create(): Module = module {
        single { get<Retrofit>().create(ThirdApi::class.java) } bind ThirdApi::class
        single { ThirdRemoteGateway(get()) }
        single { ThirdInteractor(get()) }
        viewModel { ThirdViewModel(get()) }
    }
}