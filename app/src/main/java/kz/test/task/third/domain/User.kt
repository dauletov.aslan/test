package kz.test.task.third.domain

data class User(
    val name: String,
    val phone: String,
    val username: String,
    val email: String
) {
    val fullInfo = "Username: $username\nE-mail: $email"
}