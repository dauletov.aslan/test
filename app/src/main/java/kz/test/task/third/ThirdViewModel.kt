package kz.test.task.third

import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.launch
import kz.test.task.base.BaseViewModel
import kz.test.task.third.domain.ThirdInteractor
import kz.test.task.third.domain.User

class ThirdViewModel(
    private val thirdInteractor: ThirdInteractor
) : BaseViewModel() {

    val userInfoLiveData = MutableLiveData<User>()
    val loadingLiveData = MutableLiveData<Boolean>()

    init {
        loadUser()
    }

    private fun loadUser() {
        launch {
            loadingLiveData.value = true
            userInfoLiveData.value = thirdInteractor.getUser("1")
            loadingLiveData.value = false
        }
    }
}