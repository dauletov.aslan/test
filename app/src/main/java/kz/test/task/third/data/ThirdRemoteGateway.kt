package kz.test.task.third.data

import kz.test.task.third.domain.User

class ThirdRemoteGateway(
    private val thirdApi: ThirdApi
) {

    suspend fun getUser(id: String): User = thirdApi.getUser(id).toUser()
}