package kz.test.task.third.data

import com.google.gson.annotations.SerializedName
import kz.test.task.third.domain.User

data class UserInfo(
    @SerializedName("name")
    val name: String,
    @SerializedName("phone")
    val phone: String,
    @SerializedName("username")
    val username: String,
    @SerializedName("email")
    val email: String
) {
    fun toUser(): User = User(name, phone, username, email)
}