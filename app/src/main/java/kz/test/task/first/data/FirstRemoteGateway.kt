package kz.test.task.first.data

import kz.test.task.first.domain.Photo
import kz.test.task.first.domain.Post

class FirstRemoteGateway(
    private val firstApi: FirstApi
) {

    suspend fun getPosts(): List<Post> = firstApi.getPosts().map { it.toPost() }

    suspend fun getPostsByUserId(userId: Int): List<Post> =
        firstApi.getPostsByUserId(userId).map { it.toPost() }

    suspend fun getPhoto(postId: String): Photo = firstApi.getPhoto(postId).toPhoto()
}