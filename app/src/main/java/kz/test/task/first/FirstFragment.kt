package kz.test.task.first

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kz.test.task.R
import kz.test.task.databinding.FragmentFirstBinding
import kz.test.task.first.adapter.HorizontalAdapter
import kz.test.task.first.adapter.VerticalAdapter
import kz.test.task.first.adapter.VerticalItem
import kz.test.task.viewbinding.viewBinding
import org.koin.androidx.viewmodel.ext.android.viewModel

class FirstFragment : Fragment(R.layout.fragment_first) {

    private val binding: FragmentFirstBinding by viewBinding()
    private val viewModel: FirstViewModel by viewModel()

    private val horizontalAdapter: HorizontalAdapter by lazy { HorizontalAdapter() }
    private val verticalAdapter: VerticalAdapter by lazy {
        VerticalAdapter(
            onItemClicked = { item -> viewModel.onVerticalItemClicked(item) }
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initHorizontalRecycler()
        initVerticalRecycler()

        viewModel.horizontalLoadingLiveData.observe(viewLifecycleOwner, Observer { isLoading ->
            binding.horizontalProgressBar.isVisible = isLoading
        })

        viewModel.verticalLoadingLiveData.observe(viewLifecycleOwner, Observer { isLoading ->
            binding.verticalProgressBar.isVisible = isLoading
        })

        viewModel.horizontalItemsLiveData.observe(viewLifecycleOwner, Observer { items ->
            horizontalAdapter.setItems(items)
        })

        viewModel.verticalItemsLiveData.observe(viewLifecycleOwner, Observer { items ->
            verticalAdapter.addItems(items)
        })

        viewModel.verticalItemLiveData.observe(viewLifecycleOwner, Observer { item ->
            navigateToDescriptionFragment(item)
        })
    }

    private fun initHorizontalRecycler() {
        with(binding) {
            horizontalRecyclerView.layoutManager =
                LinearLayoutManager(requireContext(), RecyclerView.HORIZONTAL, false)
            horizontalRecyclerView.adapter = horizontalAdapter
        }
    }

    private fun initVerticalRecycler() {
        with(binding) {
            verticalRecyclerView.layoutManager =
                GridLayoutManager(requireContext(), 2, RecyclerView.VERTICAL, false)
            verticalRecyclerView.adapter = verticalAdapter
            verticalRecyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
                override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                    super.onScrollStateChanged(recyclerView, newState)
                    if (!recyclerView.canScrollVertically(1)) {
                        viewModel.onRecycleEnd()
                    }
                }
            })
        }
    }

    private fun navigateToDescriptionFragment(item: VerticalItem) {
        val bundle = Bundle()
        bundle.putString("post_id", item.postId)
        bundle.putString("photo_url", item.photoUrl)
        findNavController().navigate(R.id.descriptionFragment, bundle)
    }
}