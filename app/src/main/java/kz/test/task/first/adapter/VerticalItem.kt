package kz.test.task.first.adapter

data class VerticalItem(
    val postId: String,
    val photoTitle: String,
    val photoUrl: String
)