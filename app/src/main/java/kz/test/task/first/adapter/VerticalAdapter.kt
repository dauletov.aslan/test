package kz.test.task.first.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import com.bumptech.glide.Glide
import kz.test.task.R
import kz.test.task.base.adapter.BaseAdapter
import kz.test.task.base.adapter.BaseViewHolder
import kz.test.task.databinding.ItemVerticalBinding

class VerticalAdapter(
    private val onItemClicked: (VerticalItem) -> Unit
) : BaseAdapter<VerticalItem>() {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): BaseViewHolder<VerticalItem> {
        return VerticalItemViewHolder(
            ItemVerticalBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            ),
            onItemClicked
        )
    }

    class VerticalItemViewHolder(
        private val binding: ItemVerticalBinding,
        onItemClicked: (VerticalItem) -> Unit
    ) : BaseViewHolder<VerticalItem>(binding.root, onItemClicked) {

        override fun onBind(item: VerticalItem) {
            super.onBind(item)
            with(binding) {
                Glide.with(itemView)
                    .load(item.photoUrl)
                    .placeholder(R.drawable.ic_picture)
                    .into(imageView)
                textView.text = item.photoTitle
            }
        }
    }
}