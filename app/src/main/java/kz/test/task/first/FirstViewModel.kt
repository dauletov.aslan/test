package kz.test.task.first

import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.launch
import kz.test.task.base.BaseViewModel
import kz.test.task.first.adapter.VerticalItem
import kz.test.task.first.domain.FirstInteractor

class FirstViewModel(
    private val firstInteractor: FirstInteractor
) : BaseViewModel() {

    val horizontalItemsLiveData = MutableLiveData<List<String>>()
    val verticalItemsLiveData = MutableLiveData<List<VerticalItem>>()
    val horizontalLoadingLiveData = MutableLiveData<Boolean>()
    val verticalLoadingLiveData = MutableLiveData<Boolean>()
    val verticalItemLiveData = MutableLiveData<VerticalItem>()

    private var page = 1

    init {
        loadHorizontalItems()
        loadItems()
    }

    fun onRecycleEnd() {
        loadItems()
    }

    fun onVerticalItemClicked(item: VerticalItem) {
        verticalItemLiveData.value = item
    }

    private fun loadHorizontalItems() {
        launch {
            horizontalLoadingLiveData.value = true
            val posts = firstInteractor.getPosts()

            val items = posts.map { post ->
                val photo = firstInteractor.getPhoto(post.id)
                photo.url
            }
            horizontalItemsLiveData.value = items
            horizontalLoadingLiveData.value = false
        }
    }

    private fun loadItems() {
        launch {
            verticalLoadingLiveData.value = true
            val posts = firstInteractor.getPostsByUserId(page)

            if (posts.isNotEmpty()) {
                page++
                val items = posts.map { post ->
                    val photo = firstInteractor.getPhoto(post.id)
                    VerticalItem(post.id, post.title, photo.url)
                }

                verticalItemsLiveData.value = items
            }
            verticalLoadingLiveData.value = false
        }
    }
}