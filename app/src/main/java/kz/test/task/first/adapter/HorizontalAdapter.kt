package kz.test.task.first.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import com.bumptech.glide.Glide
import kz.test.task.R
import kz.test.task.base.adapter.BaseAdapter
import kz.test.task.base.adapter.BaseViewHolder
import kz.test.task.databinding.ItemHorizontalBinding

class HorizontalAdapter : BaseAdapter<String>() {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): BaseViewHolder<String> {
        return HorizontalItemViewHolder(
            ItemHorizontalBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
        )
    }

    class HorizontalItemViewHolder(
        private val binding: ItemHorizontalBinding
    ) : BaseViewHolder<String>(binding.root) {

        override fun onBind(item: String) {
            super.onBind(item)
            with(binding) {
                Glide.with(itemView)
                    .load(item)
                    .circleCrop()
                    .placeholder(R.drawable.ic_picture)
                    .into(imageView)
            }
        }
    }
}