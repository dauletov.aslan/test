package kz.test.task.first.domain

import kz.test.task.first.data.FirstRemoteGateway

class FirstInteractor(
    private val firstRemoteGateway: FirstRemoteGateway
) {

    suspend fun getPosts(): List<Post> = firstRemoteGateway.getPosts()

    suspend fun getPostsByUserId(userId: Int): List<Post> =
        firstRemoteGateway.getPostsByUserId(userId)

    suspend fun getPhoto(postId: String): Photo = firstRemoteGateway.getPhoto(postId)
}