package kz.test.task.first.data

import com.google.gson.annotations.SerializedName
import kz.test.task.first.domain.Photo

data class PhotoInfo(
    @SerializedName("id")
    val id: String,
    @SerializedName("url")
    val url: String
) {
    fun toPhoto(): Photo = Photo(id, url)
}