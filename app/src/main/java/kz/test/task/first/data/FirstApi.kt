package kz.test.task.first.data

import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface FirstApi {
    @GET("posts")
    suspend fun getPosts(): List<PostInfo>

    @GET("posts")
    suspend fun getPostsByUserId(@Query("userId") userId: Int): List<PostInfo>

    @GET("photos/{id}")
    suspend fun getPhoto(@Path("id") postId: String): PhotoInfo
}