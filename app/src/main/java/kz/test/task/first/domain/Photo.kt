package kz.test.task.first.domain

data class Photo(val id: String, val url: String)