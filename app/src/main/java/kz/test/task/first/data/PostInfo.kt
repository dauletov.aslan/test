package kz.test.task.first.data

import com.google.gson.annotations.SerializedName
import kz.test.task.first.domain.Post

data class PostInfo(
    @SerializedName("id")
    val id: String,
    @SerializedName("title")
    val title: String,
    @SerializedName("body")
    val body: String
) {
    fun toPost(): Post = Post(id, title, body)
}