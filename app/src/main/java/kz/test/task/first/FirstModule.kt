package kz.test.task.first

import kz.test.task.di.InjectionModule
import kz.test.task.first.data.FirstApi
import kz.test.task.first.data.FirstRemoteGateway
import kz.test.task.first.domain.FirstInteractor
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.module.Module
import org.koin.dsl.bind
import org.koin.dsl.module
import retrofit2.Retrofit

object FirstModule : InjectionModule {

    override fun create(): Module = module {
        single { get<Retrofit>().create(FirstApi::class.java) } bind FirstApi::class
        single { FirstRemoteGateway(get()) }
        single { FirstInteractor(get()) }
        viewModel { FirstViewModel(get()) }
    }
}