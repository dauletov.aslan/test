package kz.test.task.first.domain

data class Post(
    val id: String,
    val title: String,
    val body: String
)