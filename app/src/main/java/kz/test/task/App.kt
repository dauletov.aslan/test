package kz.test.task

import android.app.Application
import kz.test.task.description.DescriptionModule
import kz.test.task.di.CommonModule
import kz.test.task.first.FirstModule
import kz.test.task.second.SecondModule
import kz.test.task.third.ThirdModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import timber.log.Timber

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        initKoin()
        Timber.plant(Timber.DebugTree())
    }

    private fun initKoin() {
        startKoin {
            androidContext(this@App)
            modules(
                CommonModule.create(),
                FirstModule.create(),
                DescriptionModule.create(),
                SecondModule.create(),
                ThirdModule.create()
            )
        }
    }
}