package kz.test.task.di

import android.content.Context
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.JsonParser
import kz.test.task.BuildConfig
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.core.module.Module
import org.koin.dsl.bind
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import timber.log.Timber
import java.util.concurrent.TimeUnit

object CommonModule : InjectionModule {

    override fun create(): Module = module {
        single {
            val gson = get<Gson>()

            createOkHttpClient(get())
                .apply { if (BuildConfig.DEBUG) addLoggingInterceptor(gson) }
                .build()
        } bind OkHttpClient::class

        single {
            GsonBuilder().create()
        } bind Gson::class

        single {
            Retrofit.Builder()
                .baseUrl("https://jsonplaceholder.typicode.com/")
                .addConverterFactory(GsonConverterFactory.create(get<Gson>()))
                .callFactory(get<OkHttpClient>())
                .build()
        } bind Retrofit::class
    }

    private fun createOkHttpClient(
        context: Context
    ): OkHttpClient.Builder {
        return OkHttpClient.Builder()
            .readTimeout(30L, TimeUnit.SECONDS)
            .connectTimeout(30L, TimeUnit.SECONDS)
            .cache(
                Cache(
                    context.cacheDir,
                    256 * 1024 * 1024L
                )
            )
    }

    private fun OkHttpClient.Builder.addLoggingInterceptor(gson: Gson): OkHttpClient.Builder {
        val okHttpLogTag = "OkHttp"

        val okHttpLogger = object : HttpLoggingInterceptor.Logger {
            override fun log(message: String) {
                if (!message.startsWith('{') && !message.startsWith('[')) {
                    Timber.tag(okHttpLogTag).d(message)
                    return
                }

                try {
                    val json = JsonParser.parseString(message)
                    Timber.tag(okHttpLogTag).d(gson.toJson(json))
                } catch (e: Throwable) {
                    Timber.tag(okHttpLogTag).d(message)
                }
            }
        }

        val interceptor = HttpLoggingInterceptor(okHttpLogger).apply {
            level = HttpLoggingInterceptor.Level.BODY
        }
        return addInterceptor(interceptor)
    }
}