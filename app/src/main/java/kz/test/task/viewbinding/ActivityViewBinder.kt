package kz.test.task.viewbinding

import android.view.View
import androidx.activity.ComponentActivity
import kz.test.task.R

@PublishedApi
internal class ActivityViewBinder<T>(bindingClass: Class<T>) {

    private val bindMethod by lazy {
        bindingClass.getMethod("bind", View::class.java)
    }

    @Suppress("UNCHECKED_CAST")
    fun bind(activity: ComponentActivity) =
        bindMethod.invoke(null, activity.findViewById(R.id.container)) as T
}