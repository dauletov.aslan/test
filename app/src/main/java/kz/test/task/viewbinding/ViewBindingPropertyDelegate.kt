package kz.test.task.viewbinding

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.OnLifecycleEvent
import androidx.viewbinding.ViewBinding
import kotlin.properties.ReadOnlyProperty
import kotlin.reflect.KProperty

@PublishedApi
internal abstract class ViewBindingPropertyDelegate<TComponent, TBinding : ViewBinding>(
    private val viewBinder: (TComponent) -> TBinding
) : ReadOnlyProperty<TComponent, TBinding> {

    internal var viewBinding: TBinding? = null
    private val lifecycleObserver: BindingLifecycleObserver = BindingLifecycleObserver()

    override fun getValue(thisRef: TComponent, property: KProperty<*>): TBinding {
        viewBinding?.let { return it }

        getLifecyclerOwner(thisRef).lifecycle.addObserver(lifecycleObserver)
        return viewBinder(thisRef).also { viewBinding = it }
    }

    protected abstract fun getLifecyclerOwner(thisRef: TComponent): LifecycleOwner

    private inner class BindingLifecycleObserver : LifecycleObserver {

        @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
        fun onDestroy(owner: LifecycleOwner) {
            owner.lifecycle.removeObserver(this)
            viewBinding = null
        }
    }
}