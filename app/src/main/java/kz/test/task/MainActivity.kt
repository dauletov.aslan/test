package kz.test.task

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import kz.test.task.databinding.ActivityMainBinding
import kz.test.task.viewbinding.viewBinding

class MainActivity : AppCompatActivity() {

    private val binding: ActivityMainBinding by viewBinding()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)



        with(binding) {
            bottomNavigation.setOnNavigationItemSelectedListener { item ->
                when (item.itemId) {
                    R.id.first -> findNavController(R.id.navHostFragment).navigate(R.id.firstFragment)
                    R.id.second -> findNavController(R.id.navHostFragment).navigate(R.id.secondFragment)
                    R.id.third -> findNavController(R.id.navHostFragment).navigate(R.id.thirdFragment)
                }
                true
            }
        }
    }
}