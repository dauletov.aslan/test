package kz.test.task.description.data

import kz.test.task.first.domain.Post

class DescriptionRemoteGateway(
    private val descriptionApi: DescriptionApi
) {

    suspend fun getPost(postId: String): Post = descriptionApi.getPost(postId).toPost()
}