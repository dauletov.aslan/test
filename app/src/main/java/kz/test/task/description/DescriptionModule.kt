package kz.test.task.description

import kz.test.task.description.data.DescriptionApi
import kz.test.task.description.data.DescriptionRemoteGateway
import kz.test.task.description.domain.DescriptionInteractor
import kz.test.task.di.InjectionModule
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.module.Module
import org.koin.dsl.bind
import org.koin.dsl.module
import retrofit2.Retrofit

object DescriptionModule : InjectionModule {

    override fun create(): Module = module {
        single { get<Retrofit>().create(DescriptionApi::class.java) } bind DescriptionApi::class
        single { DescriptionRemoteGateway(get()) }
        single { DescriptionInteractor(get()) }
        viewModel { DescriptionViewModel(get()) }
    }
}