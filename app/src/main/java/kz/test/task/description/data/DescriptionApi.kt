package kz.test.task.description.data

import kz.test.task.first.data.PostInfo
import retrofit2.http.GET
import retrofit2.http.Path

interface DescriptionApi {
    @GET("posts/{id}")
    suspend fun getPost(@Path("id") postId: String): PostInfo
}