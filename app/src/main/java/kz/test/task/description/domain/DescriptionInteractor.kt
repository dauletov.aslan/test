package kz.test.task.description.domain

import kz.test.task.description.data.DescriptionRemoteGateway
import kz.test.task.first.domain.Post

class DescriptionInteractor(
    private val descriptionRemoteGateway: DescriptionRemoteGateway
) {

    suspend fun getPost(postId: String): Post = descriptionRemoteGateway.getPost(postId)
}