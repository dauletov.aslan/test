package kz.test.task.description

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.bumptech.glide.Glide
import kz.test.task.R
import kz.test.task.databinding.FragmentDescriptionBinding
import kz.test.task.viewbinding.viewBinding
import org.koin.androidx.viewmodel.ext.android.viewModel

class DescriptionFragment : Fragment(R.layout.fragment_description) {

    private val binding: FragmentDescriptionBinding by viewBinding()
    private val viewModel: DescriptionViewModel by viewModel()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val postId = arguments?.getString("post_id")
        val photoUrl = arguments?.getString("photo_url")

        viewModel.loadingLiveData.observe(viewLifecycleOwner, Observer { isLoading ->
            binding.progressBar.isVisible = isLoading
        })

        viewModel.postLiveData.observe(viewLifecycleOwner, Observer { post ->
            with(binding) {
                Glide.with(view)
                    .load(photoUrl)
                    .placeholder(R.drawable.ic_picture)
                    .into(imageView)

                titleTextView.text = post.title
                descriptionTextView.text = post.body
            }
        })

        postId?.let { viewModel.onViewCreated(it) }
    }
}