package kz.test.task.description

import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.launch
import kz.test.task.base.BaseViewModel
import kz.test.task.description.domain.DescriptionInteractor
import kz.test.task.first.domain.Post

class DescriptionViewModel(
    private val descriptionInteractor: DescriptionInteractor
) : BaseViewModel() {

    val postLiveData = MutableLiveData<Post>()
    val loadingLiveData = MutableLiveData<Boolean>()

    fun onViewCreated(postId: String) {
        launch {
            loadingLiveData.value = true
            postLiveData.value = descriptionInteractor.getPost(postId)
            loadingLiveData.value = false
        }
    }
}